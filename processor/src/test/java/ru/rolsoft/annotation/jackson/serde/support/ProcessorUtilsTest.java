package ru.rolsoft.annotation.jackson.serde.support;

import org.junit.jupiter.api.Test;
import ru.rolsoft.annotation.jackson.serde.processor.ProcessorUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProcessorUtilsTest {

    @Test
    void propertyNameFromGetter() {
        assertEquals("name", ProcessorUtils.propertyName("getName"));
    }

    @Test
    void propertyNameFromGetterWithGetInName() {
        assertEquals("nameForget", ProcessorUtils.propertyName("getNameForget"));
    }

    @Test
    void propertyNameFromGetterIncorrectGetterName() {
        assertThrows(IllegalArgumentException.class, () -> ProcessorUtils.propertyName("superName"));
    }


}