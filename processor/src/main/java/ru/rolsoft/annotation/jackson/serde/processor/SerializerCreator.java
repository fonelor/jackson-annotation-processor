package ru.rolsoft.annotation.jackson.serde.processor;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.squareup.javapoet.*;
import ru.rolsoft.annotation.jackson.serde.support.ImmutableMapBuilder;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;

public class SerializerCreator {
    private static final Map<String, String> GEN_METHODS =
            new ImmutableMapBuilder<String, String>()
                    .put(String.class.getCanonicalName(), "writeStringField")
                    .put(boolean.class.getCanonicalName(), "writeBooleanField")
                    .put(Boolean.class.getCanonicalName(), "writeBooleanField")
                    .put(short.class.getCanonicalName(), "writeNumberField")
                    .put(Short.class.getCanonicalName(), "writeNumberField")
                    .put(int.class.getCanonicalName(), "writeNumberField")
                    .put(Integer.class.getCanonicalName(), "writeNumberField")
                    .put(long.class.getCanonicalName(), "writeNumberField")
                    .put(Long.class.getCanonicalName(), "writeNumberField")
                    .put(float.class.getCanonicalName(), "writeNumberField")
                    .put(Float.class.getCanonicalName(), "writeNumberField")
                    .put(double.class.getCanonicalName(), "writeNumberField")
                    .put(Double.class.getCanonicalName(), "writeNumberField")
                    .put(BigInteger.class.getCanonicalName(), "writeNumberField")
                    .put(BigDecimal.class.getCanonicalName(), "writeNumberField")
                    .put(byte[].class.getCanonicalName(), "writeBinaryField")
                    .build();

    private final ExecutableElement constructor;
    private final Map<String, PropertyInfo> properties;
    private final TypeName className;
    private final String packageName;
    private final ProcessingEnvironment processingEnv;

    public SerializerCreator(
            ExecutableElement constructor, Map<String, PropertyInfo> properties, TypeName className, String packageName, ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
        this.constructor = constructor;
        this.properties = properties;
        this.className = className;
        this.packageName = packageName;
    }

    public void build() {
        // serializer class name
        ClassName serName = ClassName.bestGuess(className + "Serializer");

        TypeSpec.Builder serializer =
                TypeSpec.classBuilder(serName)
                        .superclass(ParameterizedTypeName.get(ClassName.get(StdSerializer.class), className))
                        .addModifiers(Modifier.PUBLIC);

        MethodSpec serConstructor =
                MethodSpec.constructorBuilder()
                        .addModifiers(Modifier.PUBLIC)
                        .addCode(CodeBlock.builder().add("super($T.class);", className).build())
                        .build();

        serializer.addMethod(serConstructor);

        MethodSpec serializeMethod = buildSerialize();

        serializer.addMethod(serializeMethod);

        final JavaFile javaFile = JavaFile.builder(packageName, serializer.build()).build();

        try {
            javaFile.writeTo(processingEnv.getFiler());
        } catch (IOException e) {
            processingEnv
                    .getMessager()
                    .printMessage(
                            Diagnostic.Kind.ERROR, String.format("Unable to write generated file %s", javaFile));
            throw new RuntimeException(e);
        }
    }

    private MethodSpec buildSerialize() {
        MethodSpec.Builder builder = MethodSpec.overriding(ProcessorUtils.getMethod("serialize", JsonSerializer.class, processingEnv));

        // correct first method argument from template's T to target class
        ParameterSpec value = ParameterSpec.builder(className, "value").build();
        builder.parameters.set(0, value);

        ParameterSpec gen = ProcessorUtils.getArgumentParamSpec(builder, JsonGenerator.class);
        ParameterSpec provider = ProcessorUtils.getArgumentParamSpec(builder, SerializerProvider.class);

        // start object
        CodeBlock.Builder serializeBody =
                CodeBlock.builder().addStatement("$N.writeStartObject()", gen);

        // iterate over constructor parameters and write down them to generator
        for (VariableElement parameter : constructor.getParameters()) {
            String paramName = parameter.getSimpleName().toString();
            PropertyInfo propertyInfo = properties.get(paramName);

            if (propertyInfo == null) {
                processingEnv
                        .getMessager()
                        .printMessage(
                                Diagnostic.Kind.ERROR,
                                String.format("Unable to find getter for parameter %s", paramName));
                continue;
            }

            TypeMirror paramType = parameter.asType();

            String paramTypeName = paramType.toString();

            String method = GEN_METHODS.get(paramTypeName);

            if (method != null) {
                serializeBody.addStatement(
                        "$N.$L($S, $N.$L())", gen, method, parameter, value, propertyInfo.getGetterName());
            } else {
        /*
         non json primitive type
         final JsonSerializer<Object> dateSer =
             provider.findValueSerializer(value.getDateOfBirth().getClass());
        */
                serializeBody.addStatement(
                        "final $T $LSer = $N.findValueSerializer($N.$L().getClass())",
                        ParameterizedTypeName.get(JsonSerializer.class, Object.class),
                        parameter,
                        provider,
                        value,
                        propertyInfo.getGetterName());

                // gen.writeFieldName("dateOfBirth");
                serializeBody.addStatement("$N.writeFieldName($S)", gen, parameter);

                // dateSer.serialize(value.getDateOfBirth(), gen, provider);
                serializeBody.addStatement(
                        "$LSer.serialize($N.$L(), $N, $N)",
                        parameter,
                        value,
                        propertyInfo.getGetterName(),
                        gen,
                        provider);
            }
        }

        // end object
        serializeBody.addStatement("$N.writeEndObject();", gen);

        return builder.addCode(serializeBody.build()).build();
    }

}
