package ru.rolsoft.annotation.jackson.serde.processor;

import com.google.auto.service.AutoService;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;
import javax.tools.Diagnostic;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Required:
 *
 * <ul>
 *   <li>all fields constructor
 *   <li>getters
 * </ul>
 */
@SupportedAnnotationTypes("ru.rolsoft.annotation.jackson.serde.support.GenerateJacksonSerDe")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
@AutoService(Processor.class)
public class GenerateJacksonSerDeProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        for (TypeElement annotation : annotations) {
            final Set<? extends Element> annotated = roundEnv.getElementsAnnotatedWith(annotation);

            for (Element element : annotated) {
                processType(((TypeElement) element), roundEnv);
            }
        }

        return true;
    }

    private void processType(TypeElement clazz, RoundEnvironment roundEnv) {
        // collect getters
        Map<String, PropertyInfo> getters =
                ElementFilter.methodsIn(clazz.getEnclosedElements()).stream()
                        .filter(
                                e -> e.getParameters().size() == 0 && e.getSimpleName().toString().startsWith("get"))
                        .map(PropertyInfo::build)
                        .collect(Collectors.toMap(PropertyInfo::getPropertyName, Function.identity()));

        if (getters.isEmpty()) {
            processingEnv
                    .getMessager()
                    .printMessage(
                            Diagnostic.Kind.NOTE, "No getters in class " + clazz.getSimpleName().toString());
            return;
        }

        // verify that class has all properties constructor
        ElementFilter.constructorsIn(clazz.getEnclosedElements()).stream()
                .filter(c -> checkParameters(c, getters))
                .findFirst()
                .ifPresentOrElse(
                        executableElement -> createFiles(executableElement, getters),
                        () -> processingEnv
                                .getMessager()
                                .printMessage(
                                        Diagnostic.Kind.ERROR,
                                        String.format(
                                                "Unable to find suitable constructor for %s. "
                                                        + "There shall be all arguments constructor",
                                                clazz.getSimpleName().toString())));
    }

    private void createFiles(ExecutableElement constructor, Map<String, PropertyInfo> getters) {
        // target class name
        TypeName className = ClassName.get(constructor.getEnclosingElement().asType());

        // package name
        String packageName = "";
        final int lastDot = className.toString().lastIndexOf('.');
        if (lastDot > 0) {
            packageName = className.toString().substring(0, lastDot);
        }

        SerializerCreator serializerCreator =
                new SerializerCreator(constructor, getters, className, packageName, processingEnv);
        serializerCreator.build();

        DeserializerCreator deserializerCreator =
                new DeserializerCreator(constructor, getters, className, packageName, processingEnv);
        deserializerCreator.build();
    }


    private boolean checkParameters(
            ExecutableElement constructor, Map<String, PropertyInfo> getters) {
        List<? extends VariableElement> parameterTypes = constructor.getParameters();
        if (parameterTypes.size() != getters.size()) {
            return false;
        }

        for (VariableElement variableElement : parameterTypes) {
            String propertyName = variableElement.getSimpleName().toString();
            PropertyInfo propertyInfo = getters.get(propertyName);
            if (propertyInfo == null || !variableElement.asType().equals(propertyInfo.getType())) {
                return false;
            }
        }
        return true;
    }
}
