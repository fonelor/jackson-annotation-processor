package ru.rolsoft.annotation.jackson.serde.processor;

import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeName;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.util.ElementFilter;

public final class ProcessorUtils {
    private ProcessorUtils() {
    }

    public static String propertyName(String getterName) {
        if (!getterName.startsWith("get")) {
            throw new IllegalArgumentException("Getter name shale start with 'get'");
        }
        return Character.toLowerCase(getterName.charAt(3)) + getterName.substring(4);
    }

    public static ExecutableElement getMethod(String methodName, Class<?> aClass, ProcessingEnvironment processingEnv) {
        return ElementFilter.methodsIn(
                        processingEnv
                                .getElementUtils()
                                .getTypeElement(aClass.getCanonicalName())
                                .getEnclosedElements())
                .stream()
                .filter(m -> m.getSimpleName().toString().equals(methodName))
                .findFirst()
                .orElseThrow(
                        () -> new IllegalStateException(
                                String.format("Unable to find %s method in %s", methodName, aClass.getSimpleName())
                        ));
    }

    static ParameterSpec getArgumentParamSpec(MethodSpec.Builder builder, Class<?> typeToSearch) {
        return builder.parameters.stream()
                .filter(parameterSpec -> parameterSpec.type.equals(TypeName.get(typeToSearch)))
                .findFirst()
                .orElseThrow(
                        () ->
                                new IllegalStateException(
                                        String.format(
                                                "Unable to get parameter " + "of type %s of serialize method",
                                                typeToSearch)));
    }
}
