package ru.rolsoft.annotation.jackson.serde.processor;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeMirror;

class PropertyInfo {

  private final String getterName;
  private final String propertyName;
  private final TypeMirror type;

  static PropertyInfo build(ExecutableElement getter) {
    String name = ((Element) getter).getSimpleName().toString();
    return new PropertyInfo(name, ProcessorUtils.propertyName(name), getter.getReturnType());
  }

  private PropertyInfo(String getterName, String propertyName, TypeMirror type) {
    this.getterName = getterName;
    this.propertyName = propertyName;
    this.type = type;
  }

  public String getGetterName() {
    return getterName;
  }

  public String getPropertyName() {
    return propertyName;
  }

  public TypeMirror getType() {
    return type;
  }
}
