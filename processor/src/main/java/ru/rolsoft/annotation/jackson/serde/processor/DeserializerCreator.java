package ru.rolsoft.annotation.jackson.serde.processor;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.squareup.javapoet.*;
import ru.rolsoft.annotation.jackson.serde.support.DelegatingDeserializer;
import ru.rolsoft.annotation.jackson.serde.support.ImmutableMapBuilder;
import ru.rolsoft.annotation.jackson.serde.support.SimpleDeserializer;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DeserializerCreator {
    private static final Map<TypeName, String> SIMPLE_MAPPERS = new ImmutableMapBuilder<TypeName, String>()
            .put(TypeName.get(String.class), "getText")
            .put(TypeName.get(byte.class), "getByteValue")
            .put(TypeName.get(Byte.class), "getByteValue")
            .put(TypeName.get(short.class), "getShortValue")
            .put(TypeName.get(Short.class), "getShortValue")
            .put(TypeName.get(int.class), "getIntValue")
            .put(TypeName.get(Integer.class), "getIntValue")
            .put(TypeName.get(long.class), "getLongValue")
            .put(TypeName.get(Long.class), "getLongValue")
            .put(TypeName.get(BigInteger.class), "getBigIntegerValue")
            .put(TypeName.get(float.class), "getFloatValue")
            .put(TypeName.get(Float.class), "getFloatValue")
            .put(TypeName.get(double.class), "getDoubleValue")
            .put(TypeName.get(Double.class), "getDoubleValue")
            .put(TypeName.get(BigDecimal.class), "getDecimalValue")
            .put(TypeName.get(boolean.class), "getBooleanValue")
            .put(TypeName.get(Boolean.class), "getBooleanValue")
            .put(TypeName.get(byte[].class), "getBinaryValue")
            .build();
    private final ExecutableElement constructor;
    private final Map<String, PropertyInfo> getters;
    private final TypeName className;
    private final String packageName;
    private final ProcessingEnvironment processingEnv;
    private final List<MethodSpec> methods = new LinkedList<>();
    private final ClassName deserName;
    private final ParameterSpec p = ParameterSpec.builder(JsonParser.class, "p").build();
    private final ParameterSpec ctxt = ParameterSpec.builder(DeserializationContext.class, "ctxt").build();
    private final ParameterSpec values = ParameterSpec.builder(ParameterizedTypeName.get(Map.class, String.class, Object.class), "values").build();

    public DeserializerCreator(ExecutableElement constructor,
                               Map<String, PropertyInfo> getters,
                               TypeName className,
                               String packageName,
                               ProcessingEnvironment processingEnv) {
        this.constructor = constructor;
        this.getters = getters;
        this.className = className;
        this.packageName = packageName;
        this.processingEnv = processingEnv;
        // deserializer class name
        deserName = ClassName.bestGuess(className + "Deserializer");
    }

    public void build() {
        TypeSpec.Builder deserializer =
                TypeSpec.classBuilder(deserName)
                        .superclass(ParameterizedTypeName.get(ClassName.get(StdDeserializer.class), className))
                        .addModifiers(Modifier.PUBLIC);


        MethodSpec deserConstructor =
                MethodSpec.constructorBuilder()
                        .addModifiers(Modifier.PUBLIC)
                        .addCode(CodeBlock.builder().add("super($T.class);", className).build())
                        .build();

        deserializer.addMethod(deserConstructor);

        FieldSpec delegatingMap = buildDelegatingMap();

        FieldSpec simpleMap = buildSimpleMap();

        //private final Map<String, Object> values = new HashMap<>();
        FieldSpec values = FieldSpec.builder(ParameterizedTypeName.get(Map.class, String.class, Object.class),
                        "values", Modifier.PRIVATE, Modifier.FINAL)
                .initializer(" new $T<>()", HashMap.class)
                .build();

        deserializer.addField(delegatingMap);
        deserializer.addField(simpleMap);
        deserializer.addField(values);

        MethodSpec deserializeMethod = buildDeserialize(delegatingMap, simpleMap, values);

        deserializer.addMethod(deserializeMethod);

        methods.forEach(deserializer::addMethod);

        final JavaFile javaFile = JavaFile.builder(packageName, deserializer.build()).build();

        try {
            javaFile.writeTo(processingEnv.getFiler());
        } catch (IOException e) {
            processingEnv
                    .getMessager()
                    .printMessage(
                            Diagnostic.Kind.ERROR, String.format("Unable to write generated file %s", javaFile));
            throw new RuntimeException(e);
        }
    }

    /**
     * Generated code:
     * <code><pre>
     * private static final Map<String, SimpleDeserializer> SIMPLE_MAP =
     *          ImmutableMap.<String, SimpleDeserializer>builder()
     *                     .put("firstName", (p, ctxt) -> p.getText())
     *                     .put("lastName", (p, ctxt) -> p.getText())
     *                     .build();
     * </pre>
     * </code>
     */
    private FieldSpec buildSimpleMap() {
        CodeBlock.Builder initializerBuilder = CodeBlock.builder()
                .add("new $T<String, SimpleDeserializer>()", ImmutableMapBuilder.class);
        getters.values().stream()
                .filter(gi -> isSimple(gi.getType()))
                .forEach(getterInfo -> initializerBuilder.add(".put($S, (p, ctxt) -> p.$L())", getterInfo.getPropertyName(),
                        SIMPLE_MAPPERS.get(TypeName.get(getterInfo.getType()))));

        initializerBuilder.add(".build()");

        return FieldSpec.builder(ParameterizedTypeName.get(Map.class, String.class, SimpleDeserializer.class),
                        "SIMPLE_MAP", Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                .initializer(initializerBuilder.build())
                .build();
    }

    /**
     * Generated code:
     * <code><pre>
     * private static final Map<String, DelegatingDeserializer> DELEGATING_MAP =
     *              ImmutableMap.<String, DelegatingDeserializer>builder()
     *                     .put("dateOfBirth", OwnerExampleDeserializer::getDateOfBirth)
     *                     .build();
     * </pre></code>
     */
    private FieldSpec buildDelegatingMap() {
        CodeBlock.Builder initializerBuilder = CodeBlock.builder()
                .add("new $T<String, DelegatingDeserializer>()", ImmutableMapBuilder.class);
        getters.values().stream()
                .filter(gi -> !isSimple(gi.getType()))
                .forEach(getterInfo -> {
                    initializerBuilder.add(".put($S, $T::$L)", getterInfo.getPropertyName(), deserName,
                            getterInfo.getGetterName());
                    methods.add(buildDelegatingMethod(getterInfo));
                });

        initializerBuilder.add(".build()");

        return FieldSpec.builder(ParameterizedTypeName.get(Map.class, String.class, DelegatingDeserializer.class),
                        "DELEGATING_MAP", Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                .initializer(initializerBuilder.build())
                .build();
    }

    /**
     * Generated code:
     * <code><pre>
     *     private static void getDateOfBirth(
     *             JsonParser p, DeserializationContext ctxt, Map<String, Object> values) throws IOException {
     *         JsonDeserializer<Object> deser =
     *                 ctxt.findRootValueDeserializer(
     *                         TypeFactory.defaultInstance().constructType(LocalDate.class));
     *         p.nextToken();
     *         values.put("dateOfBirth", deser.deserialize(p, ctxt));
     *     }
     * </pre></code>
     *
     * @param getterInfo getter information to build delegating deserializing method
     */
    private MethodSpec buildDelegatingMethod(PropertyInfo getterInfo) {
        return MethodSpec.methodBuilder(getterInfo.getGetterName())
                .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                .addException(IOException.class)
                .addParameter(p)
                .addParameter(ctxt)
                .addParameter(values)
                .addCode("$T deser =\n" +
                                "                ctxt.findRootValueDeserializer(\n" +
                                "                        $T.defaultInstance().constructType($T.class));\n" +
                                "        p.nextToken();\n" +
                                "        values.put($S, deser.deserialize(p, ctxt));",
                        ParameterizedTypeName.get(JsonDeserializer.class, Object.class),
                        TypeFactory.class,
                        getterInfo.getType(),
                        getterInfo.getPropertyName())
                .build();

    }

    private boolean isSimple(TypeMirror type) {
        return SIMPLE_MAPPERS.containsKey(TypeName.get(type));
    }

    private MethodSpec buildDeserialize(FieldSpec delegatingMap, FieldSpec simpleMap, FieldSpec values) {
        MethodSpec.Builder builder = MethodSpec
                .overriding(ProcessorUtils.getMethod("deserialize", JsonDeserializer.class, processingEnv))
                .returns(className);

        ParameterSpec parser = ProcessorUtils.getArgumentParamSpec(builder, JsonParser.class);
        ParameterSpec context = ProcessorUtils.getArgumentParamSpec(builder, DeserializationContext.class);


        CodeBlock.Builder body = CodeBlock.builder();
        body.addStatement("$T jsonToken = $N.currentToken()", JsonToken.class, parser)
                .addStatement("String currentField = null")
                .beginControlFlow("while (jsonToken != JsonToken.END_OBJECT)")
                .indent()
                .beginControlFlow("switch (jsonToken)")
                .indent()
                .addStatement("case FIELD_NAME:")
                .indent()
                .addStatement("currentField = $N.currentName()", parser)
                .addStatement("$N\n" +
                        "    .getOrDefault(currentField, (p1, c, v) -> {})\n" +
                        "    .deserialize($N, $N, $N)", delegatingMap, parser, context, values)
                .addStatement("break")
                .unindent()
                .unindent()
                .addStatement("case VALUE_STRING:")
                .addStatement("case VALUE_NUMBER_FLOAT:")
                .addStatement("case VALUE_NUMBER_INT:")
                .addStatement("case VALUE_FALSE:")
                .addStatement("case VALUE_TRUE:")
                .indent()
                .addStatement("SimpleDeserializer simpleDeserializer = $N.get(currentField)", simpleMap)
                .addStatement("if (simpleDeserializer == null) {\n" +
                        "    $N.reportInputMismatch($T.class, \"Unknown property %s\", currentField);\n" +
                        "    break;\n" +
                        "}", context, className)
                .addStatement("$N.put(currentField, simpleDeserializer.deserialize($N, $N))", values, parser, context)
                .addStatement("break")
                .unindent()
                .addStatement("case START_OBJECT:")
                .indent()
                .addStatement("break")
                .unindent()
                .addStatement("default:")
                .indent()
                .addStatement("$N.reportWrongTokenException($T.class, jsonToken, \"Unexpected token\")",
                        context, className)
                .unindent()
                .endControlFlow()
                .addStatement("jsonToken = $N.nextToken()", parser)
                .unindent()
                .endControlFlow()
                .addStatement(buildReturn(values));

        builder.addCode(body.build());

        return builder.build();
    }

    private CodeBlock buildReturn(FieldSpec values) {
        CodeBlock.Builder returnBuilder = CodeBlock.builder()
                .add("return new $T(", className);
        List<? extends VariableElement> parameters = constructor.getParameters();
        for (int i = 0; i < parameters.size(); i++) {
            VariableElement parameter = parameters.get(i);
            returnBuilder.add("($T) $N.get($S)", parameter.asType(), values, parameter.getSimpleName());
            if (i < parameters.size() - 1) {
                returnBuilder.add(",");
            }
        }
        return returnBuilder
                .add(")")
                .build();
    }
}
