package ru.rolsoft.annotation.jackson.serde.support.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Test;
import ru.rolsoft.annotation.jackson.serde.user.*;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SerializerDeserializerTest {

    @Test
    void testOwnerSerializationDeserialization() throws JsonProcessingException {
        final SimpleModule simpleModule =
                new SimpleModule()
                        .addSerializer(Owner.class, new OwnerSerializer())
                        .addDeserializer(Owner.class, new OwnerDeserializer());
        final ObjectMapper objectMapper = new ObjectMapper()
                .registerModule(simpleModule)
                .registerModule(new JavaTimeModule());

        final Owner owner = new Owner("firstName", "lastName", LocalDate.now(),
                31, 31);

        final String s = objectMapper.writeValueAsString(owner);
        Owner result = objectMapper.readValue(s, Owner.class);

        assertEquals(owner, result);
    }

    @Test
    void testCarSerializationDeserialization() throws JsonProcessingException {
        final SimpleModule simpleModule =
                new SimpleModule()
                        .addSerializer(Car.class, new CarSerializer())
                        .addDeserializer(Car.class, new CarDeserializer())
                        .addSerializer(Owner.class, new OwnerSerializer())
                        .addDeserializer(Owner.class, new OwnerDeserializer());
        final ObjectMapper objectMapper = new ObjectMapper()
                .registerModule(simpleModule)
                .registerModule(new JavaTimeModule());

        final Owner owner = new Owner("Jhon", "Smith",
                LocalDate.of(1875, 7, 11),
                33, 33);
        Car car = new Car("T", LocalDate.of(1908, 6, 5), owner);

        final String s = objectMapper.writeValueAsString(car);
        Car result = objectMapper.readValue(s, Car.class);

        assertEquals(car, result);
    }
}