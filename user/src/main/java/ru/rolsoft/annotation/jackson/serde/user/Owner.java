package ru.rolsoft.annotation.jackson.serde.user;

import ru.rolsoft.annotation.jackson.serde.support.GenerateJacksonSerDe;

import java.time.LocalDate;
import java.util.Objects;

@GenerateJacksonSerDe
public class Owner {

    private final String firstName;
    private final String lastName;
    private final LocalDate dateOfBirth;
    private final int age;
    private final Integer ageB;

    public Owner(String firstName, String lastName, LocalDate dateOfBirth, int age, Integer ageB) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
        this.ageB = ageB;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    public Integer getAgeB() {
        return ageB;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Owner owner = (Owner) o;
        return age == owner.age && Objects.equals(firstName, owner.firstName) && Objects.equals(lastName, owner.lastName) && Objects.equals(dateOfBirth, owner.dateOfBirth) && Objects.equals(ageB, owner.ageB);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, dateOfBirth, age, ageB);
    }
}
