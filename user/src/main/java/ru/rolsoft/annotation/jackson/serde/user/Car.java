package ru.rolsoft.annotation.jackson.serde.user;

import ru.rolsoft.annotation.jackson.serde.support.GenerateJacksonSerDe;

import java.time.LocalDate;
import java.util.Objects;

@GenerateJacksonSerDe
public class Car {

    private final String model;
    private final LocalDate dateOfBuild;
    private final Owner owner;

    public Car(String model, LocalDate dateOfBuild, Owner owner) {
        this.model = model;
        this.dateOfBuild = dateOfBuild;
        this.owner = owner;
    }

    public String getModel() {
        return model;
    }

    public LocalDate getDateOfBuild() {
        return dateOfBuild;
    }

    public Owner getOwner() {
        return owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(model, car.model) && Objects.equals(dateOfBuild, car.dateOfBuild) && Objects.equals(owner, car.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model, dateOfBuild, owner);
    }
}
