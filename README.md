# Jackson Annotation Processor

Simple annotation processor that parses Jackson annotations and creates custom serializers/deserializers for annotated classes. The main idea is to avoid reflection