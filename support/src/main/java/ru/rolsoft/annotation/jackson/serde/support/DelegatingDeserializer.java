package ru.rolsoft.annotation.jackson.serde.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;

import java.io.IOException;
import java.util.Map;

@FunctionalInterface
public interface DelegatingDeserializer {
    void deserialize(JsonParser p, DeserializationContext ctxt, Map<String, Object> values)
            throws IOException;
}
