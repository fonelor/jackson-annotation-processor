package ru.rolsoft.annotation.jackson.serde.support;

import java.util.HashMap;
import java.util.Map;

public final class ImmutableMapBuilder<K, V> {
    private final Map<K, V> result = new HashMap<>();

    public ImmutableMapBuilder<K, V> put(K key, V value) {
        result.put(key, value);
        return this;
    }

    public Map<K, V> build() {
        return Map.copyOf(result);
    }
}
