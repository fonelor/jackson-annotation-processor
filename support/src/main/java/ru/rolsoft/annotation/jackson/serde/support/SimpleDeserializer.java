package ru.rolsoft.annotation.jackson.serde.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;

import java.io.IOException;

@FunctionalInterface
public interface SimpleDeserializer {
    Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException;
}
